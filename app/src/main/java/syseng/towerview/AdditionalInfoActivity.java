package syseng.towerview;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.transition.Scene;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.SceneView;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;

import java.io.File;

import static syseng.towerview.LabelViewActivity.checkIsSupportedDeviceOrFinish;

public class AdditionalInfoActivity extends AppCompatActivity {

    private SceneView sceneView;
    private com.google.ar.sceneform.Scene scene;
    private Node buildingNode;
    private Renderable buildingRenderable;

    private TextView buildingDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!checkIsSupportedDeviceOrFinish(this)){
            return;
        }

        setContentView(R.layout.activity_additional_info);
        sceneView = findViewById(R.id.sceneView);
        scene = sceneView.getScene();

        Annotation annotation = Annotations.getSelectedAnnotation();

        if(annotation == null){
            Toast toast =
                    Toast.makeText(this, "Oops, we don't know what to show you...",
                            Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, -1);
            toast.show();
            return;
        }
        String objectId = annotation.getObjectId();
        try {
            int renderObjectId = getResources().
                    getIdentifier("syseng.towerview:raw/" + objectId, null, null);
            renderObject(renderObjectId);
        } catch (Exception e){
            Log.e("TowerView", "Renderable not found:" + objectId);
        }

        buildingDescription = findViewById(R.id.buildingDescription);
        renderDescription(buildingDescription, annotation.getDescription());

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setTitle(annotation.getObjectName());
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        configureArButton();
    }

    private void configureArButton(){
        Button arButton = (Button) findViewById(R.id.ar_button);
        arButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdditionalInfoActivity.this,
                        ARActivity.class));
            }
        });
    }

    private void renderDescription(TextView buildingDescription, String description){
        Log.i("Tower View", description);
        buildingDescription.setText(Html.fromHtml(description,
                Html.FROM_HTML_MODE_COMPACT));
        buildingDescription.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void renderObject(int parse){
        ModelRenderable.builder()
                .setSource(this, parse)
                .build()
                .thenAccept(renderable -> addNodeToScene(renderable))
                .exceptionally(throwable -> {
                    Toast toast =
                            Toast.makeText(this, "Unable to load renderable",
                                    Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, -1);
                    toast.show();
                    return null;
                });
    }

    private void addNodeToScene(ModelRenderable model){

        if(model == null) {
            Log.w("TowerView", "addNodeToScene: model is null");
        } else {
            Log.i("TowerView", "addNodeToScene: add buildingNode...");
        }

        Node buildingNode = new RotatingNode();
        buildingNode.setParent(scene);
        Vector3 localPosition = new Vector3(0f, -2f, -3f);
        Vector3 localScale = new Vector3(1f, 1f, 1f);
        buildingNode.setLocalPosition(localPosition);
        buildingNode.setLocalScale(localScale);
        buildingNode.setName("MyBuilding");
        buildingNode.setRenderable(model);

        scene.addChild(buildingNode);
    }

    @Override
    protected void onPause(){
        super.onPause();
        sceneView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            sceneView.resume();
        } catch (Exception e) {
            Toast toast  =
                    Toast.makeText(this, "Unable to resume", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}

package syseng.towerview;

import android.app.Application;
import android.os.SystemClock;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;


public class LabelingAPI extends Application {
    public static String API_URL = "http://80.154.121.2:8000/towerview/";
    private static final boolean LOG_REQUEST = true;

    protected static LabelingAPI instance;
    protected static Integer responseCode = null;

    private static JSONObject getLabelJSON(String payload){
        String response;
        try {
            URL url = new URL(API_URL);
            Log.i("TowerView", url.toString());

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");
            Log.i("TowerView", "Connection open");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(payload);
            wr.flush();
            wr.close();

            responseCode = con.getResponseCode();
            Log.i("TowerView","\nSending 'POST' request to URL : " + url);
            Log.i("TowerView","Response Code : " + responseCode);
            if(responseCode != 200){
                Log.e("TowerView", "getLabelJSON: Got an HTTP Error" + responseCode);
                return null;
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer sb = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
            in.close();

            response = sb.toString();
            Log.i("TowerView", "getLabelJSON: " + response);
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject;

        } catch (Exception e) {
            Log.e("TowerView", "getLabelJSON failed. " + responseCode);
        }

        return null;
    }

    private static String generateRequestJSON(String payload, String height, String width){
        Log.i("TowerView", "Image dims: " + height + " " + width);
        String request = new StringBuilder()
                .append("{\"photo\":\"")
                .append(payload)
                .append("\", \"height\":"+height)
                .append(", \"width\":"+width)
                .append("}")
                .toString();
        return request;
    }

    public static List<Annotation> getLabels(String payload, String height, String width){

        String request = generateRequestJSON(payload, height, width);
        if(LOG_REQUEST) printFullRequest(request);
        JSONObject response = getLabelJSON(request);
        JSONArray buildingList = null;
        try {
            buildingList = response.getJSONArray("buildings");
        } catch (Exception e){
            Log.e("TowerView", "getLabels:Accessing Building List failed");
            return null;
        }
        Log.i("TowerView", "Recieved Labels: " + response.length());
        Log.i("TowerView", "Building List: " + buildingList.toString());

        JSONObject current;
        Annotation annotation;
        List<Annotation> annotations = new ArrayList<>();
        try {
            for (int i = 0; i < buildingList.length(); i++) {
                current = buildingList.getJSONObject(i);
                String buildingId = current.getString("buildingId");
                Double x = current.getDouble("x");
                Double y = current.getDouble("y");
                Double z = current.getDouble("z");

                annotation = new Annotation(buildingId);
                annotation.setCenter(x, y, z);
                Log.i("TowerView", "Annotation set up: " + annotation.getObjectId());

                annotation.setCenter(x, y, z);
                annotations.add(annotation);
            }
        } catch (Exception e){
            Log.e("TowerView", "Get Labels: Generating Annotation Objects failed");
            e.printStackTrace();
        }

        return annotations;

    }

    private static void printFullRequest(String request){
        for(int i=0; i<request.length()-2000; i+=2000){
            Log.i("TowerView", "request "+ i+ ": "+request.substring(i, i+2000));
            if(i==500000) SystemClock.sleep(7000);

        }
        Log.i("TowerView", "request last: "+request.substring(request.length()-2000, request.length()));
    }


}
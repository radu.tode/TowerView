package syseng.towerview;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class TakeActivity extends AppCompatActivity {

    Button takeNewPic;
    ImageView photoPreview;
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take);

        photoPreview = findViewById(R.id.photo_preview);

        // Generate a Listener for the new Pic Button and assign function
        takeNewPic = findViewById(R.id.button_newpic);
        takeNewPic.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
    }

    // This is executed after the image is taken. The result is then displayed.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            // save picture to bitmap
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            photoPreview.setImageBitmap(imageBitmap);
            showPhoto();
        }
    }

    private void showPhoto() {
        Log.i("TowerView", "Image is now displayed");
        takeNewPic.setVisibility(View.INVISIBLE);
        photoPreview.setVisibility(View.VISIBLE);
    }

    // Intent to take a picture. Launches camera app.
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
}

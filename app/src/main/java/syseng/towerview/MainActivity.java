package syseng.towerview;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.collision.Box;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Light;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static syseng.towerview.RequestDataGenerator.createBase64;


/***
 *
 */
public class MainActivity extends AppCompatActivity implements Scene.OnUpdateListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;
    private static final float CITYSCALE = 10;
    private static final float MODELSCALE = 1;

    private ArFragment arFragment;
    private ModelRenderable cityModel;
    private ModelRenderable skyboxModel;
    private ModelRenderable markerModel;
    private ModelRenderable teleporterModel;
    private TransformableNode cityNode;
    private TransformableNode cameraNode;
    private TransformableNode skyboxNode;
    private TransformableNode centerNode;
    private AnchorNode centerAnchor;
    private static Annotations annotations;
    private Light sunLight;
    private TransformableNode sunNode;
    private TransformableNode[] teleporterNodes;

    private static final Vector3 BLENDER_ORIGIN = new Vector3(12.9f,0,-7.85f);

    private static final float[][] teleporterPositions = {
            {1.16793051e+01f,  6.66785288e+00f,  2.00000003e-01f},
            { 2.67933083e+00f, -3.80095267e+00f,  2.00000003e-01f},
            {1.79313660e+01f, -5.10192919e+00f,  2.00000003e-01f},
            {-6.96398354e+00f,  1.15631447e+01f,  2.00000003e-01f},
            {-4.11755562e+00f,  1.53383398e+01f,  2.00000003e-01f},
            {-6.83836365e+00f,  4.21301812e-01f,  2.00000003e-01f}
    };



    private enum SunPosition {EAST,CENTER_EAST,CENTER,CENTER_WEST,WEST};

    private boolean startedAR = false;

    private static final float CAMERA_HEIGHT = 0.6f;

    private static final boolean IMAGE_CAPTURE_MODE = false;


    /***
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }

        setContentView(R.layout.activity_main);



        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.city_fragment);

        if(IMAGE_CAPTURE_MODE)
        {
            findViewById(R.id.ar_button_clock).setVisibility(View.INVISIBLE);
            findViewById(R.id.ar_button_map).setVisibility(View.INVISIBLE);
        }

        ModelRenderable.builder()
                .setSource(this, Uri.parse("city.sfb"))
                .build()
                .thenAccept(modelRenderable -> cityModel = modelRenderable);

        ModelRenderable.builder()
                .setSource(this, Uri.parse("skydome.sfb"))
                .build()
                .thenAccept(modelRenderable -> skyboxModel = modelRenderable);


        MaterialFactory.makeTransparentWithColor(this, new Color(android.graphics.Color.argb(10,0,0,255)))
                .thenAccept(
                        material -> {
                            teleporterModel =
                                    ShapeFactory.makeCube(new Vector3(1f, 10f, 1f),new Vector3(0.0f, .5f, 0.0f), material); });

        MaterialFactory.makeTransparentWithColor(this, new Color(1f,0,0,0.1f))
                .thenAccept(
                        material -> {
                            markerModel =
                                    ShapeFactory.makeCube(new Vector3(0.25f, 20f, 0.25f),new Vector3(0.0f, .0f, 0.0f), material); });



        arFragment.setOnTapArPlaneListener((hitResult, plane, motionEvent) -> {



            if(cityModel == null || startedAR) return;

            if(centerAnchor != null) {
                centerAnchor.getAnchor().detach();
            }

            centerAnchor = new AnchorNode(hitResult.createAnchor());
            centerAnchor.setParent(arFragment.getArSceneView().getScene());




            centerNode = new TransformableNode(arFragment.getTransformationSystem());
            centerNode.setParent(centerAnchor);
            centerNode.setLocalRotation(Quaternion.identity());

            if(IMAGE_CAPTURE_MODE){
                cameraNode = new TransformableNode(arFragment.getTransformationSystem());
                cameraNode.setParent(arFragment.getArSceneView().getScene().getCamera());
                centerNode.setParent(cameraNode);
            }

            cityNode = new TransformableNode(arFragment.getTransformationSystem());
            cityNode.setParent(centerNode);
            cityNode.setRenderable(cityModel);
            cityNode.setLocalScale(Vector3.one().scaled(CITYSCALE));
            cityNode.setLocalRotation(Quaternion.identity());
            cityNode.setCollisionShape(new Box(Vector3.zero(),Vector3.zero()));
            Vector3 pos = cityNode.getWorldPosition();
            pos.x = 0;
            pos.z = 0;
            cityNode.setLocalPosition(Vector3.zero());
            cityNode.select();

            skyboxNode = new TransformableNode(arFragment.getTransformationSystem());
            skyboxNode.setParent(centerNode);
            skyboxNode.setRenderable(skyboxModel);
            skyboxNode.getScaleController().setMaxScale(130);
            skyboxNode.setWorldScale(Vector3.one().scaled(120));
            //skyboxNode.setWorldPosition(Vector3.down().scaled(13));
            skyboxNode.setLocalRotation(Quaternion.identity());

            skyboxNode.setCollisionShape(new Box(Vector3.zero(),Vector3.zero()));
            skyboxNode.select();


            Config config = new Config(arFragment.getArSceneView().getSession());
            config.setLightEstimationMode(Config.LightEstimationMode.DISABLED);
            //config.setPlaneFindingMode(Config.PlaneFindingMode.DISABLED);
            config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
            arFragment.getArSceneView().getSession().configure(config);

            arFragment.getArSceneView().getScene().getSunlight().setEnabled(false);

            sunLight = Light.builder(Light.Type.DIRECTIONAL)
                    .setShadowCastingEnabled(true)
                    .build();

            sunNode = new TransformableNode(arFragment.getTransformationSystem());
            sunNode.setParent(centerNode);
            if(IMAGE_CAPTURE_MODE) sunNode.setParent(cameraNode);
            sunNode.setLight(sunLight);
            //sunNode.setLocalPosition(Vector3.up().scaled(100f));

            setSunPosition(SunPosition.WEST);

            arFragment.getArSceneView().getScene().getCamera().setFarClipPlane(100);
            arFragment.getPlaneDiscoveryController().hide();

            //arFragment.getArSceneView().getScene().getCamera().setProjectionMatrix(perspMat);

            startedAR = true;

            if(IMAGE_CAPTURE_MODE){
                imagePresets = ImageTestData.parseImageData(getApplicationContext()).entrySet();
                runName = "/run_1";
                dayTimeName = "degree_-90";
                generateImages();
            }

            setupTeleporters();
        });

        arFragment.getArSceneView().getScene().addOnUpdateListener(this);

    }

    public int imageCounter = 0;
    private Set<Map.Entry<String,double[][]>> imagePresets;
    private String runName = "";
    private String dayTimeName = "";

    public boolean generateImages(){
        if(imagePresets.isEmpty()) return false;

        Map.Entry<String, double[][]> entry = imagePresets.iterator().next();

        imagePresets.remove(entry);

        double[][] arr = entry.getValue();
        String foldername = entry.getKey().split("_run")[0];
        String filename = foldername + "_" + dayTimeName+"_run"+entry.getKey().split("_run")[1];

        runName = dayTimeName+"/"+foldername+"/"+filename;

        double alpha = -arr[0][2];
        double x1 = -(arr[0][0]-Math.PI/2);
        double z1 = -arr[0][1];
        double x = x1*Math.cos(alpha) + z1 * Math.sin(alpha);
        double z = x1*Math.sin(alpha) + z1 * Math.cos(alpha);

        lockRotation = ImageTestData.eulerToQuaternion(alpha,z,x);
        lockPosition = Vector3.add(new Vector3((float)-arr[1][0],(float)-arr[1][2],(float)arr[1][1]),BLENDER_ORIGIN);

        return true;
    }

    private void setupTeleporters(){

        teleporterNodes = new TransformableNode[teleporterPositions.length];

        for(int i=0; i<teleporterNodes.length; i++){
            TransformableNode teleporterNode = new TransformableNode(arFragment.getTransformationSystem());
            teleporterNode.setParent(cityNode);
            teleporterNode.setRenderable(teleporterModel);

            float[] pos = teleporterPositions[i];
            Vector3 posVec = Vector3.subtract(new Vector3(pos[0],pos[2],-pos[1]),BLENDER_ORIGIN);

            teleporterNode.setLocalPosition(posVec);
            teleporterNode.setWorldScale(new Vector3(1,15f,1));
            teleporterNode.setOnTapListener((hitTestResult, motionEvent2) -> {
                teleportTo(posVec.scaled(-1));
            });
            teleporterNode.setEnabled(false);
            teleporterNodes[i] = teleporterNode;
        }
    }

    private void teleportTo(Vector3 pos){
        pos.y = centerNode.getWorldPosition().y;
        centerNode.setLocalPosition(pos);

        cityNode.setLocalPosition(Vector3.zero());

        for(TransformableNode node: teleporterNodes)
            node.setEnabled(false);
    }

    private Set<String> annotatedBuildings = new HashSet<>();
    private Quaternion rotY90 = ImageTestData.eulerToQuaternion(Math.PI/2,0,0);
    public void addLabel(Annotation annotation){

        if(annotatedBuildings.contains(annotation.getObjectId())) return;
        annotatedBuildings.add(annotation.getObjectId());

        Coords coords = annotation.getLocationCenter();
        Vector3 position = new Vector3(coords.getX().floatValue(),10,-coords.getY().floatValue());
        position = Vector3.subtract(position,BLENDER_ORIGIN);

        TransformableNode markerNode = new TransformableNode(arFragment.getTransformationSystem());
        markerNode.setParent(cityNode);
        markerNode.setRenderable(markerModel);
        markerNode.setLocalPosition(position);
        markerNode.setOnTapListener((hitTestResult, motionEvent) ->{
            Annotations.setSelectedAnnotation(annotation);
            startActivity(new Intent(this,AdditionalInfoActivity.class));});

/*
        ViewRenderable.builder()
                .setView(this, R.layout.marker_text)
                .build()
                .thenAccept(renderable -> {
                            ((TextView) renderable.getView().findViewById(R.id.markertext)).setText(annotation.getObjectName());


                            Quaternion rot = new Quaternion();
                            rot.set(rotY90);

                            for(int i=-1; i<2; i++)
                                for(int j=-1; j<2; j++){
                                    if((i != 0 && j != 0) || (i == 0 && j == 0)) continue;
                                    TransformableNode textNode = new TransformableNode(arFragment.getTransformationSystem());
                                    textNode.setParent(markerNode);
                                    textNode.setRenderable(renderable);
                                    Vector3 pos = new Vector3(i,2.5f,j);
                                    pos.scaled(1.25f);
                                    textNode.setLocalPosition(pos);
                                    textNode.setLocalRotation(rot);
                                    rot = Quaternion.multiply(rot,rotY90);
                                }
                        }
                        );*/



    }


    int i = 0;
    float angles = 0;
    Vector3 lastDir = null;
    Vector3 lastPictureDir = null;
    Queue<Float> angleQueue = new ArrayDeque<>();


    public void motionCheck() {
        Camera cam = arFragment.getArSceneView().getArFrame().getCamera();
        float[] camDirArray = cam.getDisplayOrientedPose().getZAxis();

        Vector3 dirVec = new Vector3(camDirArray[0],camDirArray[1],camDirArray[2]);

        if(lastDir != null){
            float angle = Vector3.angleBetweenVectors(lastDir,dirVec);
            angles += angle;
            angleQueue.add(angle);
        }


        lastDir = dirVec;

        if(i>10){
            if(angles < 3)
                if(lastPictureDir == null ||  Vector3.angleBetweenVectors(lastPictureDir,dirVec) > 50){
                    lastPictureDir=dirVec;
                    sendRecognitionRequest();
                }


            angles -= angleQueue.poll();
        }
        else
            i++;
    }



    private void updateCameraHeighLock (){
            Vector3 cameraPos = arFragment.getArSceneView().getScene().getCamera().getWorldPosition();
            Vector3 centerPos = centerNode.getWorldPosition();

            centerPos.y = cameraPos.y - CAMERA_HEIGHT;

            centerNode.setWorldPosition(centerPos);
    }

    public static Quaternion lockRotation = null;
    public static Vector3 lockPosition = null;

    private void updateCameraLock (){
        com.google.ar.sceneform.Camera cam = arFragment.getArSceneView().getScene().getCamera();
        Vector3 centerPos = Vector3.zero();

        centerPos.y = lockPosition.y;
        centerPos.x = lockPosition.x;
        centerPos.z = lockPosition.z;
        centerNode.setLocalPosition(centerPos);

        cameraNode.setLocalRotation(lockRotation);

        cameraNode.setLocalPosition(Vector3.back().scaled(1f)); //zoom

        skyboxNode.getRenderable().getMaterial().setFloat3("sunDir",Quaternion.rotateVector(sunNode.getWorldRotation(),Vector3.back()));


    }

    private static final Color EAST_COLOR = new Color(1,.92f,.217f);
    private static final Color EAST_CENTER_COLOR = new Color(.53f,.632f,1f);
    private static final Color CENTER_COLOR = new Color(.139f,.63f,1f);
    private static final Color WEST_CENTER_COLOR = new Color(.928f,1f,.393f);
    private static final Color WEST_COLOR = new Color(1,.295f,.327f);

    private static SunPosition currentPos = SunPosition.CENTER;

    private void setSunPosition(SunPosition sunPos){
        currentPos = sunPos;

        switch (sunPos){
            case EAST:
                sunLight.setColor(EAST_COLOR);
                sunNode.setLocalRotation(Quaternion.lookRotation(Vector3.left(),Vector3.up()));
                setAmbientIntensity(0.2f);
                break;
            case CENTER_EAST:
                sunLight.setColor(EAST_CENTER_COLOR);
                sunNode.setLocalRotation(Quaternion.lookRotation(new Vector3(-1,-1,0),new Vector3(-1,1,0)));
                setAmbientIntensity(0.4f);
                break;
            case CENTER:
                sunLight.setColor(CENTER_COLOR);
                sunNode.setLocalRotation(Quaternion.lookRotation(Vector3.down(),Vector3.forward()));
                setAmbientIntensity(0.5f);
                break;
            case CENTER_WEST:
                sunLight.setColor(WEST_CENTER_COLOR);
                sunNode.setLocalRotation(Quaternion.lookRotation(new Vector3(1,-1,0),new Vector3(1f,1f,0f)));
                setAmbientIntensity(0.4f);
                break;
            case WEST:
                sunLight.setColor(WEST_COLOR);
                sunNode.setLocalRotation(Quaternion.lookRotation(Vector3.right(),Vector3.up()));
                setAmbientIntensity(0.15f);
                break;
        }

        HosekWilkieSkyModel.calculateSky(sunNode.getLocalRotation(),skyboxNode.getRenderable().getMaterial());
    }

    private void setAmbientIntensity(float intensity){

        for(int i=0; i<cityNode.getRenderable().getSubmeshCount(); i++){
            cityNode.getRenderable().getMaterial(i).setFloat("ambient",intensity);
        }

    }

    public void nextSunPosition(View v){
        SunPosition nextPos = SunPosition.values()[(currentPos.ordinal() + 1) % SunPosition.values().length];

        setSunPosition(nextPos);
    }

    public void showMinimap(View v){
        cityNode.setLocalPosition(Vector3.down().scaled(20));

        for(TransformableNode node: teleporterNodes)
            node.setEnabled(true);
    }

    private void sendRecognitionRequest(){
        Bitmap bitmap = RequestDataGenerator.takePhoto(arFragment,"");
        new CallServer(this).execute(bitmap);
    }

    private void setToWorldPos(TransformableNode node, Vector3 pos){
        node.setLocalPosition(pos.scaled(MODELSCALE));
    }

    float lastImage = -1;
    boolean nextImage = true;
    boolean imageTaken = false;

    @Override
    public void onUpdate(FrameTime frameTime) {
        if(!startedAR) return;

        if(IMAGE_CAPTURE_MODE)updateCameraLock();

        Quaternion lockRotInv = skyboxNode.getWorldRotation();
        skyboxNode.getRenderable().getMaterial().setFloat4("worldRot", lockRotInv.x,lockRotInv.y,lockRotInv.z,lockRotInv.w);

        if(IMAGE_CAPTURE_MODE){


            if(nextImage && !imageTaken &&  lastImage > .5){
                RequestDataGenerator.takePhoto(arFragment,runName);
                imageTaken = true;
            }
            if(nextImage && lastImage > 1){
                nextImage = generateImages();
                lastImage = 0;
                imageTaken = false;
            }

            if(nextImage )lastImage += frameTime.getDeltaSeconds();

            return;
        }

        updateCameraHeighLock();
        if(!teleporterNodes[0].isEnabled())
            motionCheck();

    }

    /**
     * Returns false and displays an error message if Sceneform can not run, true if Sceneform can run
     * on this device.
     *
     * <p>Sceneform requires Android N on the device as well as OpenGL 3.0 capabilities.
     *
     * <p>Finishes the activity if Sceneform can not run
     */
    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
            activity.finish();
            return false; 
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    private class CallServer extends AsyncTask<Bitmap, Integer, String> {

        private final boolean WRITE_BASE64_TO_FILE = true;
        private WeakReference<Context> contextRef;

        public CallServer(Context context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        protected String doInBackground(Bitmap... params){
            Bitmap bitmap = params[0];
            bitmap = Bitmap.createScaledBitmap(bitmap, 256, 530, true);
            String payload = createBase64(bitmap);
            Log.i("TowerView", "We have the payload");
            if(WRITE_BASE64_TO_FILE){
                writeToFile(payload, contextRef.get());
            }
            String height = Integer.toString(bitmap.getHeight());
            String width = Integer.toString(bitmap.getWidth());
            annotations = Annotations.setAnnotations(payload, height, width, contextRef.get());
            return "";
        }

        private void writeToFile(String data,Context context) {
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            }
            catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }

        @Override
        protected void onPostExecute(String result){
            Log.i("TowerView", "Got the following annotations stored: " + annotations.toString());

            for(Annotation annotation: annotations.getAnnotationList())
                addLabel(annotation);
        }
    }
}

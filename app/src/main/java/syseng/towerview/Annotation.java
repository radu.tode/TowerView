package syseng.towerview;

/*
 * This class holds the data for the Annotations to be added to the pboto.
 */
public class Annotation {
    private Coords locationCenter;
    private String objectName;
    private String description;
    private String objectId;


    public Annotation(String objectId){
        this.objectId = objectId;
    }

    public void setCenter(Double x, Double y, Double z){
        if(z==null){
            z=0.0;
        }
        this.locationCenter = new Coords(x, y, z);

    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getDescription() {
        return description;
    }

    public String getObjectId() {
        return objectId;
    }

    public Coords getLocationCenter() {
        return locationCenter;
    }

    public String getObjectName() {
        return objectName;
    }
}

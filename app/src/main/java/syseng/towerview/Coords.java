package syseng.towerview;

/*
 * Just a simple class to hold 2D coordinates.
 */
public class Coords {
    private Double x;
    private Double y;
    private Double z;

    public Coords(Double x, Double y, Double z){
        super();
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Double getZ() {
        return z;
    }
}

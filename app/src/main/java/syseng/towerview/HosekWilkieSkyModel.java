//https://github.com/benanders/Hosek-Wilkie/blob/master/src/main.rs

package syseng.towerview;

import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Material;

import static syseng.towerview.ArHosekSkyModelData_RGB.datasetsRGB;
import static syseng.towerview.ArHosekSkyModelData_RGB.datasetsRGBRad;

public class HosekWilkieSkyModel {

    private static final float TURBIDITY = 2f;
    private static final float GROUND_ALBEDO = 0.644f;


    public static void calculateSky(Quaternion sunDirQuat, Material skyMaterial){
        Vector3 sunDir = Quaternion.rotateVector(sunDirQuat,Vector3.back());

        float sunTheta = (float)Math.acos(clamp(sunDir.y,0.0f,1.0f));

        Vector3[] params = new Vector3[10];

        for(int i=0; i<9; i++){
            params[i] = new Vector3(
                    evaluate(datasetsRGB[0],i,9,TURBIDITY,GROUND_ALBEDO,sunTheta),
                    evaluate(datasetsRGB[1],i,9,TURBIDITY,GROUND_ALBEDO,sunTheta),
                    evaluate(datasetsRGB[2],i,9,TURBIDITY,GROUND_ALBEDO,sunTheta)
            );
        }

        params[9] = new Vector3(
                evaluate(datasetsRGBRad[0],0,1,TURBIDITY,GROUND_ALBEDO,sunTheta),
                evaluate(datasetsRGBRad[1],0,1,TURBIDITY,GROUND_ALBEDO,sunTheta),
                evaluate(datasetsRGBRad[2],0,1,TURBIDITY,GROUND_ALBEDO,sunTheta)
        );

        Vector3 s = mul_element_wise(hosekWilkie((float)Math.cos(sunTheta),0.0f,1.0f,params),params[9]);

        params[9]  = div_element_wise(params[9],Vector3.dot(s,new Vector3(0.2126f, 0.7152f, 0.0722f)));

        float sunAmount = (float)((sunDir.y/(Math.PI/2f)) % 4.0f);

        if(sunAmount > 2) sunAmount = 0.0f;

        if(sunAmount > 1) sunAmount = 2.0f - sunAmount;
        else if (sunAmount < -1.0) sunAmount = -2.0f - sunAmount;

        float normalizedSunY = 0.6f + 0.45f * sunAmount;
        params[9] = params[9].scaled(normalizedSunY);

        skyMaterial.setFloat3("avec",params[0]);
        skyMaterial.setFloat3("bvec",params[1]);
        skyMaterial.setFloat3("cvec",params[2]);
        skyMaterial.setFloat3("dvec",params[3]);
        skyMaterial.setFloat3("evec",params[4]);
        skyMaterial.setFloat3("fvec",params[5]);
        skyMaterial.setFloat3("gvec",params[6]);
        skyMaterial.setFloat3("hvec",params[7]);
        skyMaterial.setFloat3("ivec",params[8]);
        skyMaterial.setFloat3("jvec",params[9]);

        skyMaterial.setFloat3("sunDir",sunDir);
    }

    private static float evaluateSpline(double[] dataset, int start,int stride, float value){
       return (float)(1.0 *  Math.pow(1.0 - value,5) *                 dataset[start + 0 * stride] +
                5.0 *  Math.pow(1.0 - value,4) * Math.pow(value,1) * dataset[start + 1 * stride] +
                10.0 * Math.pow(1.0 - value,3) * Math.pow(value,2) * dataset[start + 2 * stride] +
                10.0 * Math.pow(1.0 - value,2) * Math.pow(value,3) * dataset[start + 3 * stride] +
                5.0 *  Math.pow(1.0 - value,1) * Math.pow(value,4) * dataset[start + 4 * stride] +
                1.0 * Math.pow(value,5) * dataset[start + 5 * stride]);
    }

    private static float evaluate(double[] dataset, int start, int stride, float turbidity, float albedo, float sunTheta){
        float evalationK = (float)Math.pow(Math.max((1.0 - sunTheta/(Math.PI/2f)),0.0),1.0/3.0);

        int turbidity0 = (int)clamp(turbidity,1,10);
        int turbidity1 = Math.min(turbidity0+1,10);
        float turbidityK = clamp(turbidity-turbidity0,0.0f,1.0f);

        int datasetA0 = start;
        int datasetA1 = (stride * 6 * 10) + start;

        float a0t0 = evaluateSpline(dataset,datasetA0+stride*6*(turbidity0-1),stride,evalationK);
        float a1t0 = evaluateSpline(dataset,datasetA1+stride*6*(turbidity0-1),stride,evalationK);
        float a0t1 = evaluateSpline(dataset,datasetA0+stride*6*(turbidity1-1),stride,evalationK);
        float a1t1 = evaluateSpline(dataset,datasetA1+stride*6*(turbidity1-1),stride,evalationK);

        return (float)(a0t0 * (1.0 - albedo) * (1.0 - turbidityK) + a1t0 * albedo * (1.0 - turbidityK) + a0t1 * (1.0 - albedo) * turbidityK + a1t1 * albedo * turbidityK);
    }

    private static Vector3 hosekWilkie(float cosTheta,float gamma, float cosGamma, Vector3[] params){
        Vector3 a = params[0];
        Vector3 b = params[1];
        Vector3 c = params[2];
        Vector3 d = params[3];
        Vector3 e = params[4];
        Vector3 f = params[5];
        Vector3 g = params[6];
        Vector3 h = params[7];
        Vector3 i = params[8];

        Vector3 pow = powV(Vector3.subtract(add_element_wise(mul_element_wise(h,h),1f),h.scaled(2f*cosGamma)),Vector3.one().scaled(1.5f));
        Vector3 chi = componentwiseDiv(1+cosGamma*cosGamma,pow);

        Vector3 hw1 = add_element_wise(mul_element_wise(a,expV(b.scaled(1f/(cosTheta+0.01f)))),1f);
        Vector3 hW = mul_element_wise(hw1,(Vector3.add(Vector3.add(Vector3.add(
                Vector3.add(c,mul_element_wise(d,expV(e.scaled(gamma)))),f.scaled(cosGamma*cosGamma)), mul_element_wise(g,chi)),
                i.scaled((float)Math.sqrt(Math.max(cosTheta,0f))))));

        return hW; 
    }

    private static Vector3 expV(Vector3 vector3){
        return new Vector3((float)Math.exp(vector3.x),(float)Math.exp(vector3.y),(float)Math.exp(vector3.z));
    }

    private static Vector3 powV(Vector3 a, Vector3 b){
        return new Vector3((float)Math.pow(a.x,b.x),(float)Math.pow(a.y,b.y),(float)Math.pow(a.z,b.z));
    }

    private static Vector3 componentwiseDiv(float f, Vector3 a){
        return new Vector3(f/a.x,f/a.y,f/a.z);
    }

    private static Vector3 mul_element_wise(Vector3 a, Vector3 b){
        return new Vector3(a.x*b.x,a.y*b.y,a.z*b.z);
    }

    private static Vector3 div_element_wise(Vector3 a, float b){
        return new Vector3(a.x/b,a.y/b,a.z/b);
    }

    private static Vector3 add_element_wise(Vector3 a, float f){
        return new Vector3(a.x+f,a.y+f,a.z+f);
    }

    private static float clamp (float value, float min, float max){
        if(value < min) return min;
        if(value > max) return max;
        return value;
    }
}

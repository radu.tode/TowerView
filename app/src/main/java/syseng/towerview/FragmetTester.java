package syseng.towerview;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.PixelCopy;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;


import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.security.Permission;
import java.util.List;

import static syseng.towerview.RequestDataGenerator.createBase64;
import static syseng.towerview.RequestDataGenerator.getBitmapFromFragment;
import static syseng.towerview.RequestDataGenerator.takePhoto;


/***
 *
 */
public class FragmetTester extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;
    public ArFragment arFragment;
    private ModelRenderable buildingRenderable;

    protected Annotations annotations;


    /***
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }

        setContentView(R.layout.activity_fragmet_tester);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.city_fragment);

        int renderObjectId = getResources().
                getIdentifier("syseng.towerview:raw/" + "andy", null, null);
        ModelRenderable.builder()
                .setSource(this, renderObjectId)
                .build()
                .thenAccept(renderable -> buildingRenderable = renderable)
                .exceptionally(
                        throwable -> {
                            Toast toast =
                                    Toast.makeText(this, "Unable to load building renderable", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return null;
                        });

        arFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    if (buildingRenderable == null) {
                        return;
                    }

                    // Create the Anchor.
                    Anchor anchor = hitResult.createAnchor();
                    AnchorNode anchorNode = new AnchorNode(anchor);
                    anchorNode.setParent(arFragment.getArSceneView().getScene());

                    // Create the transformable andy and add it to the anchor.
                    TransformableNode building = new TransformableNode(arFragment.getTransformationSystem());
                    building.setParent(anchorNode);
                    building.setRenderable(buildingRenderable);
                    building.getScaleController().setMaxScale(2.0f);
                    building.getScaleController().setMinScale(1.0f);
                    building.select();
                });

    }



    public void getBitmap(View v) throws Exception{
        Log.i("TowerView", "We are now getting the bitmap");
        Bitmap bitmap = takePhoto(this.arFragment,"");
        Log.i("TowerView", "We have the bitmap");
        new CallServer(this).execute(bitmap);
        Log.i("TowerView", "We have the called the server");
        //new CallServer(this).execute("i bimms a dummy payload", height, width);

        // RECOVER IMAGE
        //byte[] bytes = Base64.decode(payload, Base64.URL_SAFE);
        //Bitmap decodedBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        //Log.i("TowerView", decodedBitmap.toString());
        //Drawable d = new BitmapDrawable(getResources(), decodedBitmap);

        //BitmapDrawable d = new BitmapDrawable(getResources(), bitmap);
        //v.setBackground(d);
    }



    /**
     * Returns false and displays an error message if Sceneform can not run, true if Sceneform can run
     * on this device.
     *
     * <p>Sceneform requires Android N on the device as well as OpenGL 3.0 capabilities.
     *
     * <p>Finishes the activity if Sceneform can not run
     */
    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    // This belongs in the Activity from which this Functions are called
    private class CallServer extends AsyncTask<Bitmap, Integer, String> {

        private final boolean WRITE_BASE64_TO_FILE = true;
        private WeakReference<Context> contextRef;

        public CallServer(Context context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        protected String doInBackground(Bitmap... params){
            Bitmap bitmap = params[0];
            bitmap = Bitmap.createScaledBitmap(bitmap, 256, 530, true);
            String payload = createBase64(bitmap);
            Log.i("TowerView", "We have the payload");
            if(WRITE_BASE64_TO_FILE){
                writeToFile(payload, contextRef.get());
            }
            String height = Integer.toString(bitmap.getHeight());
            String width = Integer.toString(bitmap.getWidth());
            annotations = Annotations.setAnnotations(payload, height, width, contextRef.get());
            return "";
        }

        private void writeToFile(String data,Context context) {
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            }
            catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }

        @Override
        protected void onPostExecute(String result){
            try {
                Log.i("TowerView", "Got the following annotations stored: " + annotations.toString());
            } catch (Exception e){
                Log.i("TowerView", "Got nothing:");
            }
        }
    }
}

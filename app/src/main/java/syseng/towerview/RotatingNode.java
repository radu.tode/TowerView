package syseng.towerview;

import android.animation.ObjectAnimator;
import android.view.animation.LinearInterpolator;

import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.QuaternionEvaluator;
import com.google.ar.sceneform.math.Vector3;

public class RotatingNode extends Node {

    private ObjectAnimator rotationAnimation = null;
    private float degreesPerSecond = 20.0f;

    private float speedMultiplier = 1.0f;
    private float lastSpeedMultiplier = 1.0f;

    private Long animationDuration = (long) (1000*360 /(degreesPerSecond * speedMultiplier));

    @Override
    public void onUpdate(FrameTime frameTime){
        super.onUpdate(frameTime);

        // Check if rotation has been set up
        if(rotationAnimation == null){
            return;
        }

        // Nothing has changed
        if(lastSpeedMultiplier == speedMultiplier){
            return;
        }

        if(speedMultiplier == 0.0f){
            rotationAnimation.pause();
        } else {
            rotationAnimation.resume();

            float animatedFraction = rotationAnimation.getAnimatedFraction();
            rotationAnimation.setDuration(animationDuration);
            rotationAnimation.setCurrentFraction(animatedFraction);
        }
        lastSpeedMultiplier = speedMultiplier;
    }

    // Set rotation speed
    public void setDegreesPerSecond(Float degreesPerSecond){
        this.degreesPerSecond = degreesPerSecond;
    }

    @Override
    public void onActivate(){
        startAnimation();
    }

    @Override
    public void onDeactivate(){
        stopAnimation();
    }

    private void startAnimation(){
        if(rotationAnimation != null){
            return;
        }
        rotationAnimation = createAnimator();
        rotationAnimation.setTarget(this);
        rotationAnimation.setDuration(animationDuration);
        rotationAnimation.start();
    }

    private void stopAnimation(){
        if(rotationAnimation == null){
            return;
        }
        rotationAnimation.cancel();
        rotationAnimation = null;
    }

    // Returns Object animator that makes this rotate
    private ObjectAnimator createAnimator(){
        Quaternion orientation1 =
                Quaternion.axisAngle(new Vector3(0.0f, 1.0f, 0.0f), 0f);
        Quaternion orientation2 =
                Quaternion.axisAngle(new Vector3(0.0f, 1.0f, 0.0f), 120f);
        Quaternion orientation3 =
                Quaternion.axisAngle(new Vector3(0.0f, 1.0f, 0.0f), 240f);
        Quaternion orientation4 =
                Quaternion.axisAngle(new Vector3(0.0f, 1.0f, 0.0f), 360f);

        rotationAnimation = new ObjectAnimator();
        rotationAnimation.setObjectValues(orientation1, orientation2,
                orientation3, orientation4);

        rotationAnimation.setPropertyName("localRotation");

        rotationAnimation.setEvaluator(new QuaternionEvaluator());

        rotationAnimation.setRepeatCount(ObjectAnimator.INFINITE);
        rotationAnimation.setRepeatMode(ObjectAnimator.RESTART);
        rotationAnimation.setInterpolator(new LinearInterpolator());
        rotationAnimation.setAutoCancel(true);

        return rotationAnimation;
    }
}

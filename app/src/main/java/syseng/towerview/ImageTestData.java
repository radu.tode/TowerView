package syseng.towerview;

import android.content.Context;

import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class ImageTestData {

    //stepArray: [[Rotation][Position]] flip y/z (Blender Data)

    public static double[][] run_0_setp_0 =
            {{2.03992081e+00, -1.08805033e-07,  1.18349802e+00},
            { 2.63755379e+01,  1.76908169e+01,  2.00000003e-01}};

    public static double[][] run_0_setp_1 =
            {{ 2.24478507e+00, -1.08805033e-07,  1.18349802e+00},
            { 2.63755379e+01,  1.76908169e+01,  2.00000003e-01}};

    public static double[][][] run_0 = {run_0_setp_0,run_0_setp_1};

    public static double[][] run_1_setp_3 =
            {{ 1.68913305e+00,  7.59369101e-09, -7.12902904e-01},
                    { 1.09913244e+01,  5.89736176e+00,  2.00000003e-01}};

    public static double[][] run_1_setp_4 =
            {{  1.69640946e+00, -2.09036735e-08, -7.11910009e-01},
                    { 1.16793051e+01,  6.66785288e+00,  2.00000003e-01}};

    public static double[][] run_1_setp_5 =
            {{  1.70463347e+00,  6.61058692e-08, -7.10785449e-01},
                    { 1.23672857e+01,  7.43834400e+00,  2.00000003e-01}};

    public static double[][] run_1_setp_6 =
            {{ 1.88102698e+00,  6.61058692e-08, -7.10785449e-01},
                    { 1.23672857e+01,  7.43834400e+00,  2.00000003e-01}};

    public static double[][][] run_1 = {run_1_setp_3,run_1_setp_4,run_1_setp_5,run_1_setp_6};

    //http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm
    //attitude = z, heading = y, bank = x
    public static Quaternion eulerToQuaternion(double heading, double attitude, double bank){
        double c1 = Math.cos(heading/2);
        double s1 = Math.sin(heading/2);
        double c2 = Math.cos(attitude/2);
        double s2 = Math.sin(attitude/2);
        double c3 = Math.cos(bank/2);
        double s3 = Math.sin(bank/2);
        double c1c2 = c1*c2;
        double s1s2 = s1*s2;
        double w =c1c2*c3 - s1s2*s3;
        double x =c1c2*s3 + s1s2*c3;
        double y =s1*c2*c3 + c1*s2*s3;
        double z =c1*s2*c3 - s1*c2*s3;

        return new Quaternion((float)x,(float)y,(float)z,(float)w);
    }

    public static HashMap<String,double[][]> parseImageData(Context context){
        String output = "";

        try {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) return null;


            Scanner s = new Scanner(context.getAssets().open("output.txt")).useDelimiter("\\A");
            output = s.hasNext() ? s.next() : "";

        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] lines = output.split(", '");

        HashMap<String, double[][]> map = new HashMap<>();

        for(String line: lines){
            String[] lineSplit = line.split(": array");

            String name = lineSplit[0].replace("{","").replace("'","");

            String array = lineSplit[1].replaceAll("[()\\[\\] }]","");
            String[] arraySplit = array.split(",");

            double[] rotation = new double[3];
            double[] position = new double[3];

            for(int i=0; i<3; i++){
                rotation[i] = Double.parseDouble(arraySplit[i]);
                position[i] = Double.parseDouble(arraySplit[i+3]);
            }

            map.put(name,new double[][]{rotation,position});

        }

        return map;
    }
}

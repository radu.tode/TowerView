package syseng.towerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import syseng.towerview.Annotations;

public class AnnotationActivity extends AppCompatActivity {

    private Annotations annotations;
    private ImageView resultImage;
    private Bitmap analizedPicture;

    // Canvas will store what to draw on the bitmap
    private Canvas annotationCanvas;
    // Paints define how to draw it
    private Paint annotationLine = new Paint();
    private Paint annotationText = new Paint(Paint.UNDERLINE_TEXT_FLAG);

    // Bitmap represents pixels that will be displayed;
    private Bitmap mBitmap;

    // Some colors for the Annotations
    private int colorText;
    private int colorAnnotationBackground;
    private int colorMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annotation);

        // Assign color values
        colorText = ResourcesCompat.getColor(getResources(),
                R.color.colorText, null);
        colorAnnotationBackground = ResourcesCompat.getColor(getResources(),
                R.color.colorAnnotationBackground, null);
        colorMarker = ResourcesCompat.getColor(getResources(),
                R.color.colorMarker, null);

        // Now set properties of paint
        annotationLine.setColor(colorAnnotationBackground);
        annotationLine.setStrokeWidth(5);
        annotationText.setColor(colorText);
        annotationText.setTextSize(70);

        // Find ImageView
        resultImage = findViewById(R.id.result_image);

        // Note: Canvas cannot be created here. (Final size is not available)...
    }

    // Invoked by taping the View.
    // Target solution: invoked after results are received, parameter annotation list
    public void annotate(View view){

       Bitmap b = RequestDataGenerator.getBitmapFromView(view);
       String d = RequestDataGenerator.createBase64(b);

       resultImage.setImageBitmap(b);
        view.invalidate();
    }



    /*@SuppressLint("ClickableViewAccessibility")
    private void addLabels(){
        int vWidth = resultImage.getWidth();
        int vHeight = resultImage.getHeight();

        // Create the bitmap from the analyzed picture...
        analizedPicture = BitmapFactory.decodeResource(getResources(),
                R.drawable.skyline_default);
        // ...but we need a mutable bitmap
        mBitmap = analizedPicture.copy(Bitmap.Config.ARGB_8888, true);
        // Associate bitmap to ImageView
        resultImage.setImageBitmap(mBitmap);
        // Create Canvas with the bitmap
        annotationCanvas = new Canvas(mBitmap);

        // Now iterate over annotations and add them
        Coords center, top;
        for( Annotation annotation : annotations.getAnnotationList()){
            Log.i("TowerView", "Annotate Building:" + annotation.getObjectName());
            center = annotation.getLocationCenter();
            top = new Coords(center.getX(), center.getY()-100.0, 0.0);
            // Draw the label
            annotationCanvas.drawText(annotation.getObjectName(),
                    top.getX().floatValue(), top.getY().floatValue(), annotationText);
            // Draw the line
            annotationCanvas.drawCircle(center.getX().floatValue(), center.getY().floatValue(),
                    (float) 50, annotationLine);
        }
        resultImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("TowerView","Location: " + event.getX() + " , " + event.getY());

                for(Annotation annotation : Annotations.getAnnotationList()) {
                    if (Math.abs(event.getX() - annotation.getLocationCenter().getX()) < 500.0 &&
                            Math.abs(event.getY() - annotation.getLocationCenter().getY()) < 500.0) {
                        Annotations.setSelectedAnnotation(annotation);
                        startActivity(new Intent(AnnotationActivity.this,
                                AdditionalInfoActivity.class));
                        v.invalidate();
                        return true;
                    }
                }
                v.invalidate();
                return true;
            }
        });
    }*/

    private class CallServer extends AsyncTask<String, Integer, String> {

        private WeakReference<Context> contextRef;

        public CallServer(Context context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        protected String doInBackground(String... params){
            annotations = Annotations.setAnnotations(params[0], contextRef.get());
            return "";
        }

        @Override
        protected void onPostExecute(String result){

        }
    }
}

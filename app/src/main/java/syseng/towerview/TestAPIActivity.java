package syseng.towerview;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class TestAPIActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    protected Annotations annotations;
    protected ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_api);


        String nameResourceString = "goethe_school_name";
        int nameResource = getResources().getIdentifier(nameResourceString, "string", getPackageName());
        String name = getResources().getString(nameResource);
        Log.i("TowerView", "Found name: " + name);

        new CallServer(this).execute("whatever i am a placeholder for an image");
    }

    private void generateResultList(){
        ListView listView = (ListView) findViewById(R.id.resultList);

        List<String> resultList = new ArrayList<>();
        for (Annotation a : Annotations.getAnnotations()){
            resultList.add(a.getObjectName());
        }
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1, resultList);

        listView.setAdapter( arrayAdapter );
        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> l, View v, int position, long id){
        Intent intent = new Intent();
        Annotations.setSelectedAnnotation(Annotations.getAnnotations().get(position));
        startActivity(new Intent(TestAPIActivity.this,
                AdditionalInfoActivity.class));
    }


    // This belongs in the Activity from which this Functions are called
    private class CallServer extends AsyncTask<String, Integer, String> {
        private final boolean FAKE_RESPONSE = false;

        private WeakReference<Context> contextRef;

        public CallServer(Context context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        protected String doInBackground(String... params){
            if(!FAKE_RESPONSE){
                annotations = Annotations.setAnnotations(params[0], contextRef.get());
            } else {

            }
            return "";
        }

        @Override
        protected void onPostExecute(String result){
            generateResultList();
        }
    }
}

package syseng.towerview;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static syseng.towerview.LabelingAPI.getLabels;

public class Annotations {
    private static Annotations instance;

    private List<Annotation> annotationList;
    private Annotation selectedAnnotation;
    private Context context;

    private Annotations(Context context){
        this.context = context;
        this.annotationList = new ArrayList<>();
        this.instance = this;
    }

    public static List<Annotation> getAnnotations(){
        Annotations instance = Annotations.instance;
        if(instance==null){
            return null;
        }
        return Annotations.instance.annotationList;
    }

    public static Annotations setAnnotations(String payload, Context context){
        return setAnnotations(payload, "", "", context);
    }

    public static Annotations setAnnotations(String payload, String height, String width, Context context){
        Annotations instance = Annotations.instance;
        if(instance==null){
            Annotations.instance = new Annotations(context);
            instance = Annotations.instance;
        }

        List<Annotation> newAnnotations = getLabels(payload, height, width);

        if(newAnnotations==null) return null;

        instance.getAdditionalData(newAnnotations);

        for(Annotation annotation : newAnnotations){
            if(instance.annotationList.contains(annotation)) continue;
            instance.annotationList.add(annotation);
        }

        return instance;
    }

    Annotations(){
        annotationList = null;
    }

    private void getAdditionalData(List<Annotation> newAnnotations){
        for(Annotation annotation : newAnnotations){
            String buildingId = annotation.getObjectId();
            String nameResourceString = buildingId + "_name";
            Log.i("TowerView", nameResourceString);
            int nameResource =  context.getResources().
                    getIdentifier(nameResourceString, "string", "syseng.towerview");
            annotation.setObjectName(context.getResources().getString(nameResource));
            Log.i("TowerView", "Annotation name: " + annotation.getObjectName());

            String descResourceString = buildingId + "_desc";
            int descriptionResource =  context.getResources().
                    getIdentifier(descResourceString, "string", "syseng.towerview");
            annotation.setDescription(context.getResources().getString(descriptionResource));
            Log.i("TowerView", "Annotation desc: " + annotation.getDescription());
        }
    }


    public static void setAnnotationList(List<Annotation> annotationList) {
        annotationList = annotationList;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    public static void setSelectedAnnotation(Annotation selectedAnnotation) {
        instance.selectedAnnotation = selectedAnnotation;
    }

    public static Annotation getSelectedAnnotation() {
        return instance.selectedAnnotation;
    }
}

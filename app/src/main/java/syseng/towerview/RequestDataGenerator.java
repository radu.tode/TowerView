package syseng.towerview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.opengl.GLES20;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.PixelCopy;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestDataGenerator {

    public static Bitmap takePhoto(ArFragment f, String filename){
        ArSceneView arSceneView = f.getArSceneView();
        Bitmap bitmap = Bitmap.createBitmap(arSceneView.getWidth(), arSceneView.getHeight(),
                Bitmap.Config.ARGB_8888);

        HandlerThread handlerThread = new HandlerThread("PixelCopier");
        handlerThread.start();

        PixelCopy.request(arSceneView, bitmap, (int copyResult) -> {
            if (copyResult == PixelCopy.SUCCESS) {
                //Log.i("TowerView", "Getting the bitmap: "+ bitmap.toString());
                createBase64(bitmap);
                try {
                    saveBitmapToDisk(bitmap,filename);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("TowerView", "Getting the bitmap failed miserably");
            }

            handlerThread.quitSafely();
        }, new Handler(handlerThread.getLooper()));

        if(bitmap == null) Log.e("TowerView", "Bitmap is null");
        Log.i("TowerView", "Image is taken now");
        return bitmap;
    }


    public static Bitmap getBitmapFromFragment(ArFragment f){
        //return getBitmapFromView(f.getView());
        Image i=null;
        try {
            i = f.getArSceneView()
                    .getSession()
                    .update()
                    .acquireCameraImage();
        } catch (Exception e){
            e.printStackTrace();
        }
        if(i == null){
            Log.e("TowerView", "Image is null");
        }
        ByteBuffer buffer0 = i.getPlanes()[0].getBuffer();
        ByteBuffer buffer1 = i.getPlanes()[1].getBuffer();
        ByteBuffer buffer2 = i.getPlanes()[2].getBuffer();
        byte[] compByteArray = new byte[buffer0.capacity()+ buffer1.capacity()+ buffer2.capacity()];
        buffer0.get(compByteArray, 0, buffer0.capacity());
        buffer1.get(compByteArray, buffer0.capacity(), buffer1.capacity());
        buffer2.get(compByteArray, buffer0.capacity()+buffer1.capacity(), buffer2.capacity());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Log.i("TowerView", byteArrayOutputStream.toByteArray().toString());
        YuvImage yuvImage = new YuvImage(compByteArray, ImageFormat.NV21,
                i.getWidth(), i.getHeight(),null);
        yuvImage.compressToJpeg(new Rect(0,0,i.getWidth(), i.getHeight()), 75, byteArrayOutputStream);
        byte[] byteForBitmap = byteArrayOutputStream.toByteArray();
        Log.i("TowerView", byteForBitmap.toString());

        Bitmap bitmap = BitmapFactory.decodeByteArray(byteForBitmap, 0, byteForBitmap.length);
        if(bitmap==null) Log.e("TowerView", "Bitmap is null.");
        return bitmap;
    }



    public static Bitmap getBitmapFromView(View v){
        int height = v.getMeasuredHeight();
        int width = v.getMeasuredWidth();

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        v.layout(0,0, v.getWidth(), v.getHeight());
        v.draw(canvas);
        Log.i("TowerView", "combineImages: width: " + v.getWidth());
        Log.i("TowerView", "combineImages: height: " + v.getHeight());

        return bitmap;
    }

    public static String createBase64(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        String encodedData =  Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.NO_WRAP);
        //Log.i("TowerView", "generateRequest: " + encodedData);
        return encodedData;
    }

    private static String generateFilename(String filename) {
        String date = (filename.length() == 0)?
                new SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.getDefault()).format(new Date()): filename;
        return Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES) + File.separator + "Sceneform/" + date + ".png";
    }

    private static void saveBitmapToDisk(Bitmap bitmap, String filename) throws IOException {

        filename = generateFilename(filename);
        File out = new File(filename);
        if (!out.getParentFile().exists()) {
            out.getParentFile().mkdirs();
        }
        try (FileOutputStream outputStream = new FileOutputStream(filename);
             ByteArrayOutputStream outputData = new ByteArrayOutputStream()) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputData);
            outputData.writeTo(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException ex) {
            throw new IOException("Failed to save bitmap to disk", ex);
        }
    }
}

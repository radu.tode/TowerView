package syseng.towerview;

import com.fasterxml.jackson.core.JsonParser;

import org.json.JSONObject;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import io.specto.hoverfly.junit.dsl.HttpBodyConverter;
import io.specto.hoverfly.junit.rule.HoverflyRule;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.HttpBodyConverter.json;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.notFound;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static io.specto.hoverfly.junit.dsl.matchers.HoverflyMatchers.equalsToJson;
import static org.junit.Assert.*;

public class LabelingAPITest {

    private static final String SERVICE_ADDRESS = "http://172.22.40.147:8000";
    private static final String SERVICE_PATH = "/towerview/";

    private static final HttpBodyConverter FIND_ONE_BUILDING = json("one");
    private static final HttpBodyConverter FIND_TOW_BUILDINGS = json("two");
    private static final HttpBodyConverter FIND_NOTHING = json("null");
    private static final HttpBodyConverter SERVER_ERROR = json("server_error");

    @ClassRule
    public static HoverflyRule hoverflyRule = HoverflyRule.inSimulationMode(dsl(
            service(SERVICE_ADDRESS)
                    .post(SERVICE_PATH)
                    .body(equalsToJson(FIND_ONE_BUILDING))
                    .willReturn(success("{\"buildings\":[{\"buildingId\":\"fake\",\"x\":0.0,\"y\":0.0,\"z\":0.0}]}", "application/json")),
            service(SERVICE_ADDRESS)
                    .post(SERVICE_PATH)
                    .body(equalsToJson(FIND_TOW_BUILDINGS))
                    .willReturn(success("{\"buildings\":[{\"buildingId\":\"fake\",\"x\":0.0,\"y\":0.0,\"z\":0.0},{\"buildingId\":\"fake2\",\"x\":1.3,\"y\":0.0,\"z\":0.0}]}", "application/json")),
            service(SERVICE_ADDRESS)
                    .post(SERVICE_PATH)
                    .body(equalsToJson(FIND_NOTHING))
                    .willReturn(success("{\"buildings\":[]}", "application/json")),
            service(SERVICE_ADDRESS)
                    .post(SERVICE_PATH)
                    .body(equalsToJson(SERVER_ERROR))
                    .willReturn(notFound())
    ));

    @Test
    public void findOneResult() throws Exception {

        List<Annotation> response = LabelingAPI.getLabels(FIND_ONE_BUILDING.body(), "", "");
        assertEquals(response.size(), 1);
        assertEquals(response.get(0).getObjectId(), "fake");
    }

    @Test
    public void findTwoResults() throws Exception {

        List<Annotation> response = LabelingAPI.getLabels(FIND_TOW_BUILDINGS.body(),"","");
        assertEquals(response.size(), 2);
        assertEquals(response.get(0).getObjectId(), "fake");
        assertEquals(response.get(1).getObjectId(), "fake2");
        assertEquals(response.get(1).getLocationCenter().getX().toString(), "1.3");
    }

    @Test
    public void findNothing() throws Exception {

        JSONObject json = new JSONObject("{}");
        List<Annotation> response = LabelingAPI.getLabels(FIND_NOTHING.body(),"","");
        assertEquals(response.size(), 0);
    }

    @Test
    public void checkServerError() throws Exception {

        List<Annotation> response = LabelingAPI.getLabels(SERVER_ERROR.body(),"","");
        assertEquals(response, null);
    }

}
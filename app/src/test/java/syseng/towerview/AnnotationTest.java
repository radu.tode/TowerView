package syseng.towerview;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AnnotationTest {

    @Test
    public void annotationContent(){

        Annotation annotation = new Annotation("buildingId");
        assertThat(annotation.getObjectId(), is("buildingId"));

        annotation.setCenter(1.0, 2.0, null);
        Coords center = annotation.getLocationCenter();
        assertThat(center, instanceOf(Coords.class));
        assertThat(center.getZ(), is(0.0));

        annotation.setDescription("<b>Test</b>");
        assertThat(annotation.getDescription(), is("<b>Test</b>"));

    }

}
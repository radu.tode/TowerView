package syseng.towerview;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CoordsTest {

    @Test
    public void coordsStorageAndRetrieval(){

        Coords coords = new Coords(1.0 , 2.2, -3.3);
        assertThat(coords.getX(), is(1.0));
        assertThat(coords.getY(), is(2.2));
        assertThat(coords.getZ(), is(-3.3));

    }

}
# TowerView

Imagine you want to visit a city but you have no idea what places you should visit …
The solution is simple … Its called Towerview

With our application you are able to walk through a virtual copy of the city you are interested in and visit different places.Just hold the focus of your camera to you point of interest and the app checks if this building is worth a visit.

(This repo contains the Android App for the application, the Backend, MachineLearning and training data generation code can be found in the respective repositories)

## User requirements

1.	User can select his position inside the virtual city
2.	Application shall present the user some 3D view of the city where he can focus his buildings of interest
3. 	Application shall be able to detect the important buildings in the current view and provide the user detailed information about it.
4. 	Application shall present the User a 3D model of the bulldings 
5. 	The application shall be able to present some AR model of this 	building

## Use Cases
<img src="use_cases.PNG" width="250px">

## Screenshots
<img src="city.PNG" width="250px">
<img src="detailed.PNG" width="250px">
<img src="inspect.PNG" width="250px">